<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class listaController extends AbstractController
{
    /**
     * @Route("/listaTareas",name="listaTareas")
     */
    public function listaTareas(Request $request)
    {
        $tarea = $request->get("tarea");
        if(!file_exists("tareas.json"))
        {
            $tareas = [];
            if($tarea)
            {
                $tareas[] = ["tarea" => $tarea, "id"=>0];
            }
        }
        else
        {
            $tareas = json_decode(file_get_contents("tareas.json"),true);
            if($tarea)
            {
                if($tareas==""){
                    $tareas = [];
                    $n = 0;
                }    
                else{
                    $n = -1;
                    foreach($tareas as $tar)
                    {
                        $id = $tar["id"];
                        if($id>=$n)
                        {
                            $n = $id+1;
                        }
                    }
                }
                $tareas[] = ["tarea" => $tarea, "id"=>$n];
            }
        }
        file_put_contents("tareas.json",json_encode($tareas));
        return $this->render("listaTareas.html.twig",["tareas"=>$tareas]);
        /*if($tarea)
        {
            else{
                $tareas = json_decode(file_get_contents("tareas.json"),true);
                $n = -1;
                foreach($tareas as $tarea1){
                    $id = $tarea1["id"];
                    if($id>=$n)
                    {
                        $n = $id+1;
                    }
                }
            }
            $tareas[] = ["tarea" => $tarea, "id"=>$n];
            file_put_contents("tareas.json",json_encode($tareas));
            
        }
        else
        {
            return $this->render("listaTareas.html.twig",["tareas"=>[]]);
        }*/
        
        
    }
    
    /**
     * @Route("/nuevaTarea",name="nuevaTarea")
     */
    /*public function anadirTarea(Request $request)
    {
        $tarea = $request->request["tarea"];

        if($tarea)
        {
            if(!file_exists("tareas.json"))
            {
                $tareas = [];
                $n = 0;
            }
            else
            {
                $tareas = json_decode(file_get_contents("tareas.json"));
                $n = -1;
                foreach($tareas as $id => $tarea){
                    if($id>$n)
                        $n = $id+1;
                }
            }
            $tareas["$n"] = ["tarea" => $tarea, "id"=>$n];
            file_put_contents("tareas.json",json_encode($tareas));
        }
        return $this->render("listaTareas.html.twig",$tareas);
    }
    
    public function quitarLista(Request $request)
    {
        $id = $request->request["id"];
        if($id)
        {
            $tareas = json_decode(file_get_contents("tareas.json"));

            unset($tareas["$id"]);

            file_put_contents('tareas.json',json_encode($tareas));
        }
        return $this->render("listaTareas.html.twig",$tareas);
    }*/

}